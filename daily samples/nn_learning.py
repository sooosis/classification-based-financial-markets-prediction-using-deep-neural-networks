import numpy as np
import pandas as pd
from keras.models import Sequential, Model
from keras.layers import Dense, Input
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from keras.optimizers import Adam, SGD
import copy
import keras
import tensorflow as tf
from sklearn.datasets import make_multilabel_classification




EPOCH = 100

ACC_train = []
ACC_test = []
class PredictionCallback(keras.callbacks.Callback):
    def __init__(self, X_train,X_test,Y_test,Y_train):
        super(PredictionCallback, self).__init__()
        self.y_test = Y_test
        self.X_train = X_train
        self.X_test = X_test
        self.Y_train = Y_train

    def on_epoch_end(self, epoch, logs={}):
        #print('arya')
        #y_pred = self.model.predict(self.validation_data[0])
        Y_pred = recreate_y(self.model.predict(self.X_test))
        ACC_test.append(evaluation(self.y_test,Y_pred)[3])
        Y_pred = recreate_y(self.model.predict(self.X_train))

        ACC_train.append(evaluation(self.Y_train,Y_pred)[3])

        #print('prediction: {} at epoch: {}'.format(y_pred, epoch))


def recreate_y(Y_):
    Y = np.array(Y_)
    y_final = []
    for i in range(0,np.shape(Y)[1]):
        y_temp = []
        for j in range(0, 18):
            y_temp.append(np.argmax(Y[j, i, :])-1)
        y_final.append(y_temp)
    return np.array(y_final)

def evaluation(y_true,y_predict):
    f1 = []
    prec = []
    recall = []
    acc = []
    for i in range(0,18):
        label_true = y_true[:,i]
        label_pred = y_predict[:, i]
        acc.append(accuracy_score(label_true, label_pred))
        f1_p = precision_recall_fscore_support(label_true, label_pred, average='macro')
        prec.append(f1_p[0])
        recall.append(f1_p[1])
        f1.append(f1_p[2])
    return np.mean(f1), np.mean(prec), np.mean(recall), np.mean(acc)



def y_constructor(Y):
    y_final = []
    for y in Y:
        y_temp = []
        for tag in y:
            if tag == -1:
                y_temp.append([1,0,0])
            elif tag == 0:
                y_temp.append([0,1,0])
            else:
                y_temp.append([0,0,1])
        y_final.append(y_temp)

    return np.array(y_final)


X_train = np.load('X_train.npy')
X_test = np.load('X_test.npy')
Y_train = np.load('Y_train.npy')
Y_test = np.load('Y_test.npy')
Y_test_base = copy.copy(Y_test)
Y_train_base = copy.copy(Y_train)

Y_train = y_constructor(Y_train)



print(Y_train)
Y_test = y_constructor(Y_test)
#print(Y_test)
state_input = Input(shape=(np.shape(X_train)[1],))
dense1 = Dense(1000, activation='relu')(state_input)
dense2 = Dense(100, activation='relu')(dense1)
dense3 = Dense(100, activation='relu')(dense2)
dense4 = Dense(100, activation='relu')(dense3)
dense5 = Dense(100, activation='relu')(dense4)
outpu1 =  Dense(3, activation='softmax')(dense5)
outpu2 =  Dense(3, activation='softmax')(dense5)
outpu3 =  Dense(3, activation='softmax')(dense5)
outpu4 =  Dense(3, activation='softmax')(dense5)
outpu5 =  Dense(3, activation='softmax')(dense5)
outpu6 =  Dense(3, activation='softmax')(dense5)
outpu7 =  Dense(3, activation='softmax')(dense5)
outpu8 =  Dense(3, activation='softmax')(dense5)
outpu9 =  Dense(3, activation='softmax')(dense5)
outpu10 =  Dense(3, activation='softmax')(dense5)
outpu11 =  Dense(3, activation='softmax')(dense5)
outpu12 =  Dense(3, activation='softmax')(dense5)
outpu13 =  Dense(3, activation='softmax')(dense5)
outpu14 =  Dense(3, activation='softmax')(dense5)
outpu15 =  Dense(3, activation='softmax')(dense5)
outpu16 =  Dense(3, activation='softmax')(dense5)
outpu17 =  Dense(3, activation='softmax')(dense5)
outpu18 =  Dense(3, activation='softmax')(dense5)
model = Model(inputs=state_input, outputs=[outpu1,outpu2,outpu3,outpu4,outpu5,outpu6,outpu7,
                                         outpu8,outpu9,outpu10,outpu11,outpu12,outpu13,outpu14,outpu15
    ,outpu16,outpu17,outpu18])

model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001))
history = model.fit(X_train, [Y_train[:,0,:],Y_train[:,1,:],Y_train[:,2,:],Y_train[:,3,:],Y_train[:,4,:],Y_train[:,5,:]
                    ,Y_train[:,6,:],Y_train[:,7,:],Y_train[:,8,:],Y_train[:,9,:],Y_train[:,10,:],Y_train[:,11,:],
                    Y_train[:,12,:],Y_train[:,13,:],Y_train[:,14,:],Y_train[:,15,:],Y_train[:,16,:],Y_train[:,17,:]],
                    epochs=EPOCH,batch_size=32, shuffle=True,callbacks=[PredictionCallback(X_train,X_test,Y_test_base,Y_train_base)])
#print(np.shape(model.predict(X_test)))
Y_pred = recreate_y(model.predict(X_test))
#print('shape: ', np.shape(Y_pred))
#print(evaluation(Y_test_base,Y_pred))
plt.plot(np.arange(0,EPOCH),ACC_train,label = 'Train')
plt.plot(np.arange(0,EPOCH),ACC_test,label = 'Validation')
plt.legend()
plt.xlabel('Epoch')
plt.ylabel('Mean Accuracy')
plt.savefig('Curves.png')
print('EVALUATION : ', evaluation(Y_test_base, Y_pred))

#plt.show()

#y_test = print(evaluation(Y_train,model.predict(Y_test)))


#print(np.shape(Y_test))

