
import yfinance as yf
import datetime
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

stocks = yf.Tickers("msft aapl goog amzn fb tsla jnj jpm stt apo trow kkr ntrs ben nad nuv noah vrts")

ticktime = '8y'
interval = '1h'
minterval = '1d'
google = stocks.tickers.GOOG.history(period=ticktime, interval=interval)
microsoft = stocks.tickers.MSFT.history(period=ticktime, interval=interval)
amazon = stocks.tickers.AMZN.history(period=ticktime, interval=interval)
tesla = stocks.tickers.TSLA.history(period=ticktime, interval=interval)
jandj = stocks.tickers.JNJ.history(period=ticktime, interval=interval)
jp = stocks.tickers.JPM.history(period=ticktime, interval=interval)
fb = stocks.tickers.FB.history(period=ticktime, interval=interval)
st = stocks.tickers.STT.history(period=ticktime, interval=interval)
appl = stocks.tickers.AAPL.history(period=ticktime, interval=interval)
apo = stocks.tickers.APO.history(period=ticktime, interval=interval)
bam = stocks.tickers.TROW.history(period=ticktime, interval=interval)
kkr = stocks.tickers.KKR.history(period=ticktime, interval=interval)
ntrs = stocks.tickers.NTRS.history(period=ticktime, interval=interval)
ben = stocks.tickers.BEN.history(period=ticktime, interval=interval)
nad = stocks.tickers.NAD.history(period=ticktime, interval=interval)
nuv = stocks.tickers.NUV.history(period=ticktime, interval=interval)
noah = stocks.tickers.NOAH.history(period=ticktime, interval=interval)
vrts = stocks.tickers.VRTS.history(period=ticktime, interval=interval)

assets = [google, microsoft, amazon, tesla, jandj, jp, fb, st, appl, apo, bam, kkr, ntrs, ben, nad, nuv, noah, vrts]

for i in assets:
    i['ret'] = i["Close"].diff(+1)


def moving_mean(asset, c):
    res = []

    for i in range(5, 100):
        res.append(asset['Close'].rolling(i).mean().iloc[c])

    return res


def lagged_prices(asset, c):
    res = []

    for i in range(1, 100):
        res.append(asset['Close'].shift(periods=i).iloc[c])

    return res


def correlation(g, m, a, t, jn, jpi, f, s, ap, apoo, ba, kk, ntr, bn, na, nu, noa, vrt):
    stocks_df = pd.DataFrame()

    stocks_df['goog'] = g['ret']
    stocks_df['msft'] = m['ret']
    stocks_df['tsla'] = t['ret']
    stocks_df['amzn'] = a['ret']
    stocks_df['jnj'] = jn['ret']
    stocks_df['jp'] = jpi['ret']
    stocks_df['fb'] = f['ret']
    stocks_df['stt'] = s['ret']
    stocks_df['appl'] = ap['ret']

    stocks_df['apo'] = apoo['ret']
    stocks_df['bam'] = ba['ret']
    stocks_df['kkr'] = kk['ret']
    stocks_df['ntrs'] = ntr['ret']
    stocks_df['ben'] = bn['ret']
    stocks_df['nad'] = na['ret']
    stocks_df['nuv'] = nu['ret']
    stocks_df['noah'] = noa['ret']
    stocks_df['vrts'] = vrt['ret']

    correl = stocks_df.corr()

    res = []
    for i in correl:
        res += correl[i].tolist()

    return res


def prices(c):
    res = []

    for i in assets:
        res.append(i['Open'].iloc[c])

    return res


def create_data(asset):
    c = 0
    data = []

    for i, j in asset.iterrows():

        g = google.iloc[max(0, c - 10):c + 1]
        m = microsoft.iloc[max(0, c - 10):c + 1]
        a = amazon.iloc[max(0, c - 10):c + 1]
        t = tesla.iloc[max(0, c - 10):c + 1]
        jpi = jp.iloc[max(0, c - 10):c + 1]
        jn = jandj.iloc[max(0, c - 10):c + 1]
        f = fb.iloc[max(0, c - 10):c + 1]
        s = st.iloc[max(0, c - 10):c + 1]
        ap = appl.iloc[max(0, c - 10):c + 1]

        apoo = apo.iloc[max(0, c - 10):c + 1]
        ba = bam.iloc[max(0, c - 10):c + 1]
        kk = kkr.iloc[max(0, c - 10):c + 1]
        ntr = ntrs.iloc[max(0, c - 10):c + 1]
        bn = ben.iloc[max(0, c - 10):c + 1]
        na = nad.iloc[max(0, c - 10):c + 1]
        nu = fb.iloc[max(0, c - 10):c + 1]
        noa = noah.iloc[max(0, c - 10):c + 1]
        vrt = vrts.iloc[max(0, c - 10):c + 1]

        frame = correlation(g, m, a, t, jn, jpi, f, s, ap, apoo, ba, kk, ntr, bn, na, nu, noa, vrt) + prices(c)

        for k in assets:
            frame += moving_mean(k, c)
        for k in assets:
            frame += lagged_prices(k, c)

        for k in assets:
            if ((k['ret'].iloc[c])/(k['ret'].iloc[c] + k['Close'].iloc[c]) >= .02):
                frame.append(1)
            elif ((k['ret'].iloc[c])/(k['ret'].iloc[c] + k['Close'].iloc[c]) <= -0.007):
                frame.append(-1)
            else:
                frame.append(0)

        data.append(np.array(frame))

        c += 1

    data = np.array(data)
    data = data[~np.isnan(data).any(axis=1)]

    y = np.array(data[:, -18:])
    x = np.array(data[:, 0:-18])

    return x, y


x, y = create_data(assets[0])



r, c = x.shape
x_train = x[0:int(r * 7 / 10)]

scaler = StandardScaler()
scaler.fit(x_train)
x_train = scaler.transform(x_train)
x = scaler.transform(x)

x_train = x[0:int(r * 7 / 10)]
x_test = x[int(r * 7 / 10):r + 1]

y_train = y[0:int(r * 7 / 10)]
y_test = y[int(r * 7 / 10):r + 1]

print(x_train[10] , y_train[235])
np.save('X_train' , x_train)
np.save('X_test' , x_test)
np.save('Y_train' , y_train)
np.save('Y_test' , y_test)